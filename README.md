# What The Phage - snakemake port

>This repository has been deprecated. No future development is provisioned
>The original WtP supports execution without sudo rights via
>singularity containers.
>Steps to do so are described [here](doc/wtp_singularity.md).

## Motivation

The original idea is already implemented with `nextflow` and `docker`.
We cannot install docker or run docker containers on our system due to
permissions restrictions (for now), so
we translated the original workflow into `snakemake`.
All credits for tool discovery and original implementation, go to the
[WTP](https://github.com/replikation/What_the_Phage) people.

## Installation

You will need a working
[conda installation](https://docs.conda.io/en/latest/miniconda.html).

Create a new conda environment using the `requirements.yml` file
```
# Clone this repo
$ git clone https://git.science.uu.nl/n.pappas/project-003-virometools.git

# Get in it
$ cd project-003-virometools

# Create an isolated environment with conda
$ conda env create -n <myenv> --file=requirements.yml
```
This will fetch snakemake and all required dependencies for running the
snakemake workflow.

For all modules, separate conda environments are handled by snakemake itself.
Dependencies for each environment are specified in `workflow/envs`.

### Databases set up

Each tool wrapped here comes with its own database dependencies. These are **not**
handled automagically for you here. You will need to follow instructions,
according to each tool. The final location of each resulting database is
required when running the workflow, so take note where you store everything

## Input

Fasta files containing assembled contigs.
> Important note
>
>Currently, these are stored in one directory and they follow the naming
>convention `<sample>.scaffolds.fasta`. The sample name is heavily used as
>a wildcard within snakemake rules, so make sure it is unique.
>
>Example: A `data_dir` with files named `sample1.scaffolds.fasta`,
>`sample2.scaffolds.fasta`

## Running the workflow

An example `config.yml` file is provided, under in `config`.

Once you fill this in with the required values, running the analysis with
```
$ conda activate <myenv> # Don't forget to use the environment from above


$ snakemake -p -j 10 --use-conda --config config/config.yml
```

should suffice.

# Output

An example output looks like this
```
output_dir
├── SRR1303490
│   ├── CAT
│   ├── deepvirfinder
│   ├── SRR1303490.supervenn.png
│   ├── vibrant
│   └── virsorter
└── SRR1303502
    ├── CAT
    ├── deepvirfinder
    ├── SRR1303502.supervenn.png
    ├── vibrant
    └── virsorter
```

Results are stored per sample, within specific directories for each tool.
The `<sample_name>.supervenn.png` summarizes all results for a sample, in a
graph showing the intersecting sets of identified viral sequences per tool,
looking like this:

![an image](./doc/images/SRR1303490.supervenn.png)
