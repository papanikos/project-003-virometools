rule virsorter:
	input:
		fasta_in= DATA_DIR + "/{sample}.scaffolds.fasta",
	output:
		virsorter_signal= OUTPUT_DIR + "/{sample}/virsorter/{sample}_global-phage-signal.csv"
	log:
		err=OUTPUT_DIR + "/{sample}/virsorter/.{sample}.stderr",
		out=OUTPUT_DIR + "/{sample}/virsorter/.{sample}.stdout"
	threads: 4
	params:
		virsorter_data=config['virsorter_data'],
		virsorter_outdir=OUTPUT_DIR + "/{sample}/virsorter"
	conda:
		"../envs/virsorter.yml"
	shell:
		"wrapper_phage_contigs_sorter_iPlant.pl --fna {input.fasta_in} "
		"--db 1 "
		"--data-dir {params.virsorter_data} "
		"--ncpu {threads} "
		"--wdir {params.virsorter_outdir} "
		"-d {wildcards.sample} "
		"1>{log.out} 2>{log.err}"

rule filter_virsorter:
	input:
		 virsorter_out=rules.virsorter.output.virsorter_signal,
	output:
		  virsorter_filtered_tsv=OUTPUT_DIR + "/{sample}/virsorter/{sample}.contigs.passed.tsv"
	params:
		  categories='1,2',
		  dataset_prefix="{sample}"
	shell:
		 "python scripts/process_virsorter.py -i {input.virsorter_out} "
		 "-c {params.categories} "
		 "-d {params.dataset_prefix} "
		 "-o {output.virsorter_filtered_tsv}"

rule get_virsorter_contigs:
	input:
		filtered_tsv = rules.filter_virsorter.output.virsorter_filtered_tsv
	output:
		contigs_list = OUTPUT_DIR + "/{sample}/virsorter/{sample}.contigs.list"
	shell:
		 "tail -n+2 {input.filtered_tsv} | cut -f1 > {output.contigs_list}"