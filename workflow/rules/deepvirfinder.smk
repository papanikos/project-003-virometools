rule deepvirfinder:
	input:
		fasta_in= DATA_DIR + "/{sample}.scaffolds.fasta",
	log:
		err=OUTPUT_DIR + "/{sample}/deepvirfinder/dvf.stderr",
		out=OUTPUT_DIR + "/{sample}/deepvirfinder/dvf.stdout"
	threads: 4
	params:
		dvf_installdir=config["dvf_installdir"],
		dvf_outdir=OUTPUT_DIR + "/{sample}/deepvirfinder",
		length_cutoff=500
	output:
		dvf_outfile=OUTPUT_DIR + "/{sample}/deepvirfinder/{sample}.scaffolds.fasta_gt500bp_dvfpred.txt"
	conda:
		"../envs/dvf.yml"
	shell:
		"python {params.dvf_installdir}/dvf.py -m {params.dvf_installdir}/models "
		"-i {input.fasta_in} "
		"-c {threads} -l {params.length_cutoff} -o {params.dvf_outdir} "
		"1>{log.out} 2>{log.err}"

rule filter_dvf:
    input:
        dvf_out=rules.deepvirfinder.output.dvf_outfile
    output:
        dvf_filtered_tsv = OUTPUT_DIR + "/{sample}/deepvirfinder/{sample}.contigs.passed.tsv"
    params:
        s_thresh= 0.9,
        p_thresh= 0.05,
        l_thresh = 500,

    shell:
         "python scripts/process_dvf.py -i {input.dvf_out} "
         "-s {params.s_thresh} "
         "-p {params.p_thresh} "
         "-l {params.l_thresh} "
         "-o {output.dvf_filtered_tsv}"

rule get_dvf_contigs:
	input:
		filtered_tsv = rules.filter_dvf.output.dvf_filtered_tsv
	output:
		contigs_list = OUTPUT_DIR + "/{sample}/deepvirfinder/{sample}.contigs.list"
	shell:
		"tail -n+2 {input.filtered_tsv} | cut -f 1 > {output.contigs_list}"
