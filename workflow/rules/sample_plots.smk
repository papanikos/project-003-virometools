rule make_sample_plots:
	# input:
	# 	sample_dir = OUTPUT_DIR + '/{sample}',
	output:
		supervenn_plot = OUTPUT_DIR + '/{sample}/{sample}.supervenn.png'
    params:
        sample_dir = OUTPUT_DIR + '/{sample}'
	shell:
		"python scripts/supervenn_plot.py "
		"-s {params.sample_dir} -o {output.supervenn_plot}"
