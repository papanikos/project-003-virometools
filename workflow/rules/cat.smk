# Construct db strings
CAT_DB = '_'.join([config['cat_db_prefix'], 'CAT_database'])
CAT_TAX = '_'.join([config['cat_db_prefix'], 'taxonomy'])

rule cat_annotate:
    input:
        fasta_in = DATA_DIR + "/{sample}.scaffolds.fasta"
    output:
        contig2classification = OUTPUT_DIR + "/{sample}/CAT/{sample}.contig2classification.txt",
    params:
        out_prefix = OUTPUT_DIR + "/{sample}/CAT/{sample}",
        cat_taxonomy = CAT_TAX,
        cat_db = CAT_DB
    log:
        err = OUTPUT_DIR + "/{sample}/CAT/.{sample}.CAT_annotate.stderr",
        out = OUTPUT_DIR + "/{sample}/CAT/.{sample}.CAT_annotate.stdout"
    threads: 10
    conda:
        "../envs/cat.yml"
    shell:
        "CAT contigs -c {input.fasta_in} -t {params.cat_taxonomy} "
        "-d {params.cat_db} "
        "--nproc {threads} "
        "-o {params.out_prefix} 1>{log.out} 2>{log.err}"

rule cat_add_names:
    input:
        classified = rules.cat_annotate.output.contig2classification
    output:
        named = OUTPUT_DIR + "/{sample}/CAT/{sample}.names_official.txt"
    params:
        cat_taxonomy = CAT_TAX,
    log:
        err = OUTPUT_DIR + "/{sample}/CAT/.{sample}.CAT_add_names.stderr",
        out = OUTPUT_DIR + "/{sample}/CAT/.{sample}.CAT_add_names.stdout"
    threads: 1
    conda:
        "../envs/cat.yml"
    shell:
        "CAT add_names -i {input.classified} "        
        "--only_official "
        "-t {params.cat_taxonomy} "
        "-o {output.named} "
        "1>>{log.out} 2>{log.err}"

rule cat_summarise:
    input:
        fasta_in = DATA_DIR + "/{sample}.scaffolds.fasta",
        named = rules.cat_add_names.output.named
    output:
        summary = OUTPUT_DIR + "/{sample}/CAT/{sample}.summary.txt"
    log:
        err = OUTPUT_DIR + "/{sample}/CAT/.{sample}.CAT_summarise.stderr",
        out = OUTPUT_DIR + "/{sample}/CAT/.{sample}.CAT_summarise.stdout"
    conda:
        "../envs/cat.yml"
    threads: 1
    shell:
        "CAT summarise -c {input.fasta_in} "
        "-i {input.named} "
		"-o {output.summary} "
        "1>{log.out} 2>{log.err}"

rule process_cat:
    input:
        summary = rules.cat_add_names.output.named
    output:
        cat_filtered_tsv = OUTPUT_DIR + "/{sample}/CAT/{sample}.contigs.passed.tsv"
    shell:
         "grep 'virus' {input.summary} > {output.cat_filtered_tsv}"

rule get_cat_contigs:
    input:
        filtered_tsv = rules.process_cat.output.cat_filtered_tsv
    output:
        contigs_list = OUTPUT_DIR + "/{sample}/CAT/{sample}.contigs.list"
    shell:
        "cut -f 1 {input.filtered_tsv} > {output.contigs_list}"