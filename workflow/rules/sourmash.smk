rule sourmash_compute:
    input:
        fasta_in= DATA_DIR + "/{sample}.scaffolds.fasta",
    output:
        sourmash_sig = OUTPUT_DIR + "/{sample}/sourmash/{sample}.sig"
    params:
        k = 21,
        scaled = 100,
    log:
        err = OUTPUT_DIR + "/{sample}/sourmash/.{sample}.sm_compute.err",
        out = OUTPUT_DIR + "/{sample}/sourmash/.{sample}.sm_compute.out"
    conda:
        "../envs/sourmash.yml"
    shell:
        "sourmash compute "
        "--scaled {params.scaled} "
        "-k {params.k} "
        "-o {output.sourmash_sig} {input.fasta_in}"

rule sourmash_search:
    input:
        fasta_sig = rules.sourmash_compute.output.sourmash_sig,
        phages_sig = config['sourmash_sig']
    output:
        sourmash_matches = OUTPUT_DIR + "/{sample}/sourmash/{sample}.matches.tsv"
    log:
        err = OUTPUT_DIR + "/{sample}/sourmash/.{sample}.sm_search.err",
        out = OUTPUT_DIR + "/{sample}/sourmash/.{sample}.sm_search.out"
    conda:
        "../envs/sourmash.yml"
    shell:
        "sourmash compute "
        "-o {output.sourmash_matches} {input.fasta_sig}"
