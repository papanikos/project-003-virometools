rule vibrant:
    input:
        fasta_in = DATA_DIR + "/{sample}.scaffolds.fasta",
    output:
        vibrant_summary_raw = OUTPUT_DIR + "/{sample}/vibrant/VIBRANT_{sample}.scaffolds/VIBRANT_results_{sample}.scaffolds/VIBRANT_summary_results_{sample}.scaffolds.tsv",
        vibrant_summary = OUTPUT_DIR + "/{sample}/vibrant/{sample}.contigs.passed.tsv"
    log:
        err = OUTPUT_DIR + "/.{sample}/vibrant/{sample}.stderr",
        out = OUTPUT_DIR + "/.{sample}/vibrant/{sample}.stdout"
    threads: 10
    params:
        vibrant_outdir = OUTPUT_DIR + "/{sample}/vibrant",
        vibrant_db = config["vibrant_db"]
    conda:
        "../envs/vibrant.yml"
    shell:
        "VIBRANT_run.py -i {input.fasta_in} "
        "-folder {params.vibrant_outdir} "
        "-t {threads} "
        "-d {params.vibrant_db}/databases "
        "-m {params.vibrant_db}/files "
        "1>{log.out} 2>{log.err} && "
        "cp {output.vibrant_summary_raw} {output.vibrant_summary}"

rule get_vibrant_contigs:
    input:
        filtered_tsv = rules.vibrant.output.vibrant_summary
    output:
        contigs_list = OUTPUT_DIR + "/{sample}/vibrant/{sample}.contigs.list"
    shell:
        "tail -n+2 {input.filtered_tsv} | cut -f1 > {output.contigs_list}"

