from supervenn import supervenn
from pathlib import Path
import matplotlib.pyplot as plt
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='Plot sample results')

    optionalArgs = parser._action_groups.pop()

    requiredArgs = parser.add_argument_group("required arguments")
    requiredArgs.add_argument('-s', '--sample-dir',
                              dest='sampledir',
                              type=lambda p: Path(p).resolve(strict=True),
                              required=True,
                              help="Sample directory where results are stored per tool")
    requiredArgs.add_argument('-o', '--output-plot',
                              dest='outfile',
                              required=True,
                              type=lambda p: Path(p).resolve(),
                              help="FIle to save the plot")


    parser._action_groups.append(optionalArgs)

    return parser.parse_args()

def get_contigs_set_from_file(filepath):
    with open(filepath, 'r') as fin:
        contigs_set = {line.strip() for line in fin}
    return contigs_set


def get_sets(sample_dir):
    sample_name = Path(sample_dir).name
    tools, sets = [], []
    for x in Path(sample_dir).iterdir():
        if x.is_dir():
            tool = x.name
            contigs_list_file = '{}.contigs.list'.format(sample_name)
            contigs_list_fp = x.joinpath(contigs_list_file)
            contigs_set = get_contigs_set_from_file(contigs_list_fp)
            tools.append(tool)
            sets.append(contigs_set)
    return tools, sets


def supervenn_plot(outfile, sets, tools):
    supervenn(sets, tools, side_plots=False, sets_ordering='minimize gaps')
    figure = plt.gcf()
    figure.savefig(outfile)

def main():
    args = parse_args()
    tools, sets = get_sets(args.sampledir)
    supervenn_plot(args.outfile, sets, tools)


if __name__ == '__main__':
    main()
