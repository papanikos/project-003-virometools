import pandas as pd
import pathlib

def samplesheet_to_dict(samplesheet_tsv):
    # Convert the input value to path, and check if it is there
    samplesheet_path = pathlib.Path(samplesheet_tsv).resolve(strict=True)
    # Make a data frame
    df = pd.read_csv(samplesheet_path, sep='\t', index_col='samples')
    # Convert it to a dictionary { sample : {'fasta': path_to_fasta}}
    dic = df.to_dict(orient='index')
    return dic
