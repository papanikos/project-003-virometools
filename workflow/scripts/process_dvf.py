#!/usr/bin/env python

from pathlib import Path
import pandas as pd
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='Filter deepvirfinder results')

    optionalArgs = parser._action_groups.pop()

    requiredArgs = parser.add_argument_group("required arguments")
    requiredArgs.add_argument('-i', '--input-file',
                              dest='infile',
                              type=lambda p: Path(p).resolve(strict=True),
                              required=True,
                              help="A tsv file containing deepvirfinder predictions")
    requiredArgs.add_argument('-o', '--output-file',
                              dest='outfile',
                              required=True,
                              type=lambda p: Path(p).resolve(),
                              help="File path to write the results in ")
    optionalArgs.add_argument('-s', '--score-filter',
                              dest='s_thresh',
                              required=False,
                              type=float,
                              default=0.9,
                              help="Score threshold for contigs to be included [0.9]",
                              metavar="[0-1]"
                              )
    optionalArgs.add_argument('-p', '--pval-filter',
                              dest='p_thresh',
                              required=False,
                              type=float,
                              default=0.05,
                              help="Pvalue threshold for contigs to be included [0.05]",
                              metavar="[0-1]")
    optionalArgs.add_argument('-l', '--len-filter',
                              dest='len_thresh',
                              required=False,
                              type=int,
                              default=500,
                              help='Length threshold for contigs to be included [500]',
                              metavar="> 0")

    parser._action_groups.append(optionalArgs)

    return parser.parse_args()


def filter_dvf_table(dvf_txt, s_thresh=0.9, p_thresh=0.05, len_thresh=500):
    """
    Filter the output of deepvirfinder for the specified thresholds
    :param dvf_txt: Raw output of dvf
    :param s_thresh: Score threshold to apply (inclusive)
    :param p_thresh: Pvalue threshold to apply (inclusive)
    :param len_thresh: Length threshold to apply (positive integer)
    :return: A pd.DataFrame with the contigs passing the filters or None
    """
    df = pd.read_csv(dvf_txt, sep='\t')
    df = df.rename(columns={'name': 'contig_id'})
    # Filter the input in a new df
    filtered_df = df[(df['score'] >= s_thresh)
                     & (df['pvalue'] <= p_thresh)
                     & (df['len'] >= len_thresh)]
    if filtered_df.shape[0] > 0:
        return filtered_df
    else:
        return None


def main():
    args = parse_args()
    result_filtered = filter_dvf_table(args.infile,
                                       args.s_thresh,
                                       args.p_thresh,
                                       args.len_thresh)
    if result_filtered is not None:
        result_filtered.to_csv(args.outfile,
                                index=False,
                                header=True,
                                sep='\t')

    else:
        with open(args.outfile, 'w') as fout:
            fout.write('None\n')


if __name__ == '__main__':
    main()
