#!/usr/bin/env python

from pathlib import Path
import pandas as pd
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='Filter virsorter results')

    optionalArgs = parser._action_groups.pop()

    requiredArgs = parser.add_argument_group("required arguments")
    requiredArgs.add_argument('-i', '--input-file',
                              dest='infile',
                              type=lambda p: Path(p).resolve(strict=True),
                              required=True,
                              help="A tsv file containing virsorter predictions")
    requiredArgs.add_argument('-o', '--output-file',
                              dest='outfile',
                              required=True,
                              type=lambda p: Path(p).resolve(),
                              help="File path to write the results in ")
    optionalArgs.add_argument('-c', '--categories',
                              dest="categories",
                              type=str,
                              default='1,2',
                              help="Include contigs that fall into these categories [1,2]",
                              metavar="[1,2,3,4,5,6]"
                              )
    optionalArgs.add_argument('-d', '--dataset-code',
                              dest="dataset_prefix",
                              required=False,
                              default='VIRSorter',
                              help="Prefix used when running VIRSorter (with the option -d). "
                                   "You know what this is if you used it",
                              )
    parser._action_groups.append(optionalArgs)
    return parser.parse_args()

def place_back_the_dot(virsorter_name, dot_index):
    original_string = '.'.join([virsorter_name[:dot_index], virsorter_name[dot_index+1:]])
    return original_string


def parse_virsorter_signal(input_file, prefix_used=None):
    data = {}
    with open(input_file, 'r') as fin:
        current_cat = 0
        number_of_hashes = 0
        for line in fin:
            # Keep track of categories
            if line.startswith("##"):
                number_of_hashes += 1
                # When we read both lines that start with ##
                if number_of_hashes == 2:
                    # Reset the counter
                    number_of_hashes = 0
                    # Switch category
                    current_cat += 1
                    # ..and
                    continue
            else:
                fields = line.split(',')
                contig_id = fields[0].strip()
                # Remove this from the contigs to bring them back to original
                if prefix_used is not None:
                    contig_prefix = "{}_".format(prefix_used)
                    contig_id = contig_id.replace(contig_prefix, '')
                    # pfff
                    dot_here = contig_id.rfind('_') # Find the last of occurenece of '_'
                    contig_id = place_back_the_dot(contig_id, dot_here)
                if '-circular' in contig_id:
                    contig_id = contig_id.replace('-circular', '')
                if contig_id not in data:
                    data[contig_id] = current_cat
                else:
                    print("Weird.. One contig in two categories")
                    print("Current category: {}".format(current_cat))
                    print("Contig is already in {}".format(data[contig_id]))
    return data


def create_df_from_data(data_dic):
    # Put the data in a data frame
    virsorter_df = pd.DataFrame.from_dict(data_dic, orient="index", columns=['category'])
    # Rename the columns to be a bit more informative
    virsorter_df = virsorter_df.reset_index().rename(columns={'index': 'contig_id'})
    return virsorter_df


def filter_virsorter_df(virsorter_df, keep_categories=[1,2]):
    filtered_df = virsorter_df.loc[virsorter_df['category'].isin(keep_categories)]
    if filtered_df.shape[0] != 0:
        return filtered_df
    else:
        return None

def main():
    args = parse_args()
    keep_these = [int(i) for i in args.categories.split(',')]
    if len(keep_these) == 0:
        print("Categories argument should be like '1' or '1,3,5' ")
        exit(1)

    input_data = parse_virsorter_signal(args.infile, prefix_used=args.dataset_prefix)
    categories_df = create_df_from_data(input_data)

    filtered_df = filter_virsorter_df(categories_df, keep_these)
    if filtered_df is not None:
        filtered_df.to_csv(args.outfile, index=False, header=True, sep='\t')

if __name__ == '__main__':
    main()
