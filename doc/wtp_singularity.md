# Run original What_the_Phage pipeline

## Install requirements

1. Create a new conda environment with `singularity` and `nextflow`

```
# Full versions are the ones I used
# You can change the name
$ conda create -n wtp -c conda-forge singularity=3.6 nextflow=20.07.1
```

2. Activate the environment
```
$ conda activate wtp
```

3. Create a directory where everything will be stored and get in it

```
# Note the prefixed (wtp) shows I have activated the environment
(wtp)$ mkdir wtp_test
(wtp)$ cd wtp_test
```

4. Set some `singularity` related environment variables.

Note that this will force all related intermediated files to be written in
these paths.

* Make sure there is enough disk space. The whole thing really blows up to almost 200GB.
The run directory `wtp_test` goes up to *70G* with all databases and singularity images and
my `/tmp` goes up to *180G* (started clean!)

```
# DON'T FORGET TO CHANGE THE "/path/to/my/wtp_test" TO THE ACTUAL LOCATION
(wtp)$ export SINGULARITY_CACHE=/path/to/my/wtp_test
(wtp)$ export SINGULARITY_LOCALCACHE=/path/to/my/wtp_test
(wtp)$ export SINGULARITY_TMPDIR=/path/to/my/wtp_test # It will default to /tmp
```

If all went well, you can already run the help
```
(wtp)$ nextflow run replikation/What_the_Phage --help
```

---

## Setting up the thing

To setup databases and containers you need to run the workflow with the `--setup` option.
This might fail several times but you will get there eventually.

```
# Only run this if you actually want to set it up
(wtp)$ nextflow run replikation/What_the_Phage -r v0.9.0 --setup -profile local,singularity
```

For TBB users with access on our cluster:

* Containers : `/hosts/linuxhome/fungus/nikos2/wtp_test/singularity_images`
* Databases  : `/hosts/linuxhome/fungus/nikos2/wtp_test/nextflow-autodownload-databases`

>TO DO
>
>If this works, move DBs to a central location

Use these locations with the appropriate options when running it

## Testing the installation

The command below downloads and runs 8 samples described in the paper.
```
(wtp)$ nextflow run replikation/What_the_Phage -r v0.9.0 -profile test,local,singularity
```

## Run on your data

1. Prepare a directory where all fasta files you want to analyze are stored, if you don't have one already.
I assume this is `/path/to/fasta_dir`.

2. Make sure you have followed the installation requirements from above, with a working
`conda` environment with `singularity` and `nextflow` and singularity environment variables set.

3. You can use the databases from above.

3. Run this:
```
(wtp)$ nextflow run replikation/What_the_Phage \
		-r v0.9.0 \
		--cores 8 \ # Change this accordingly
		--fasta /path/to/fasta_dir/*.fasta \
		-profile local,singularity \
		--database /hosts/linuxhome/fungus/nikos2/wtp_test/nextflow-autodownload-databases\
		--workdir $SINGULARITY_TMPDIR \
		--cachedir /hosts/linuxhome/fungus/nikos2/wtp_test/singularity_images 
```

If you want to run specific tools/subwrorflows only, modify the command with [some of these](https://github.com/replikation/What_the_Phage#workflow-control).

